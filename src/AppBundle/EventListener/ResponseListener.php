<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener
{
    use ContainerAwareTrait;

    /**
     * @param FilterResponseEvent $event
     *
     * @return void
     */
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        $request = $this->container->get('request_stack')->getMasterRequest();

        if (!$request || !$request->query->getBoolean('log')) {
            return;
        }

        $this->container->get('app.util.http_dumper')->dumpFromRequestAndResponse(
            $request,
            $event->getResponse()
        );
    }
}