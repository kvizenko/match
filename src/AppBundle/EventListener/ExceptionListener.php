<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    use ContainerAwareTrait;

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        if ($exception instanceof HttpExceptionInterface) {
            return;
        }

        $request  = $this->container->get('request_stack')->getMasterRequest();
        $response = $event->getResponse();

        if ($request && $request->query->getBoolean('log')) {
            $this->container->get('app.util.http_dumper')->dumpFromRequestAndResponse($request, $response);
        }
    }
}