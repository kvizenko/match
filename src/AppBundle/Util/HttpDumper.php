<?php

namespace AppBundle\Util;

use AppBundle\Entity\HttpLog;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpDumper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return void
     */
    public function dumpFromRequestAndResponse(Request $request, Response $response): void
    {
        $log = new HttpLog();

        $log
            ->setDate(new \DateTime())
            ->setResponse($response)
            ->setRequest($request)
            ->setStatusCode($response->getStatusCode())
            ->setUrl($request->getUri())
            ->setClientIp($request->getClientIp())
        ;

        $this->em->persist($log);
        $this->em->flush($log);
    }
}