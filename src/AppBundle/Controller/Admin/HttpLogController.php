<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\HttpLog;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Ip;

class HttpLogController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/admin/http-log", name="http-log")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $ipFilter = $request->query->get('client_ip');

        if ($ipFilter) {
            $validator  = $this->container->get('validator');
            $constraint = new Ip();

            if ($validator->validate($ipFilter, $constraint)->count() !== 0) {
                return $this->render(
                    'admin/http-log.html.twig',
                    ['data' => null, 'error' => 'Bad ip address!']
                );
            }
        }

        /** @var EntityRepository $repository */
        $repository = $this->container->get('doctrine')->getRepository(HttpLog::class);

        $qb = $repository->createQueryBuilder('l');

        if ($ipFilter) {
            $qb
                ->where('l.clientIp = :clientIp')
                ->setParameter('clientIp', $ipFilter)
            ;
        }

        // TODO pagination
        $data = $qb
            ->orderBy('l.date', 'DESC')
            ->getQuery()
            ->getArrayResult()
        ;

        return $this->render(
            'admin/http-log.html.twig',
            ['data' => $data, 'error' => null]
        );
    }
}